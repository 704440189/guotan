'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_BASE_URL: '"http://47.97.157.115:9292"'  //访问地址
  //API_BASE_URL: '"http://192.168.1.14:9292"'  //访问地址47.97.157.115:9292
  //API_BASE_URL: '"localhost:8080"'
})
