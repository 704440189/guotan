import Vue from 'vue'
import 'babel-polyfill'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant'
import 'vant/lib/index.css'
import http from '@/modules/http'
import storage from '@/modules/storage'
import moment from 'moment'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
import { Toast } from 'vant'
import VideoPlayer from 'vue-video-player'
require('video.js/dist/video-js.css')
require('vue-video-player/src/custom-theme.css')

Vue.use(VideoPlayer)


Vue.use(Vant)
Vue.use(VueAwesomeSwiper, /* { default global options } */)

Vue.config.productionTip = false


// 获取传过来的数据
function getUrl() {
  // 截取url中的数据
  /**
  * tempStr 格式是http://域名/路由?token=value&imgUrl=value...
  */
  let tempStr = window.location.href
  //let tempStr='https://www.baidu.com/s?token=12345678&id=34'
  /**
  * tempArr 是一个字符串数组 格式是["key=value", "key=value", ...]
  */
  let tempArr = tempStr.split('?')[1] ? tempStr.split('?')[1].split('&') : []
  /**
  * returnArr 是要返回出去的数据对象 格式是 { key: value, key: value, ... }
  */
  let returnArr = {}
  tempArr.forEach(element => {
    returnArr[element.split('=')[0]] = element.split('=')[1]
  })
  /* 输出日志 */
  Object.assign(returnArr,{url:tempStr})
  return returnArr
}

Vue.prototype.Toast=Toast
Vue.prototype.$url = getUrl()
Vue.prototype.$http = http
Vue.prototype.$storage = storage
Vue.prototype.$moment=moment


new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
