import axios from 'axios'
import { Toast, Dialog } from 'vant'
const $http = axios.create({
  baseURL: process.env.API_BASE_URL,
  timeout:50000
})
// 拦截器
$http.interceptors.request.use((config) => {
  Toast.loading({
    duration: 0,       // 持续展示 toast
    forbidClick: true, // 禁用背景点击
    loadingType: 'spinner',
    message: '加载中...'
  });
  return config // config是ajax的配置文件，如果不返回，则ajax则不能请求,的不到数据
})

$http.interceptors.response.use((res) => {
  Toast.clear()
  handleSuccessRes(res.data)
  return res.data
}, async (error) => {
  await handleResponseError(error)
  return false
})
async function handleSuccessRes (data) {
  if (data.status !=1) { // token过期
    await Dialog.alert({
      title: '错误提示',
      message: data.message
    }).then(()=>{
      if(data.status==-1){
         window.open(`http://guotan_tokenPast`,'_self')
      }
    })
  }
}

async function handleResponseError (error) {
  await Dialog.alert({
    title: '错误提示',
    message: error.response.data.message
  })
}
export default $http
