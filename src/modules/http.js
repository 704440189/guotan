import request from '@/modules/request'
import storage from '@/modules/storage'
import axios from 'axios'
import qs from 'qs'
import {Dialog, Toast } from 'vant'

async function http(params){
    let result=null
    if(params.method=='get'){
         result = await request.get(params.url,qs.stringify(params.data))
    }
    if(params.method=='post'){
         result = await request.post(params.url, qs.stringify(params.data))
    }
    if(params.method=='posts'){
          result = await request.post(params.url,params.data)
     }    
     return result
}

export default http
