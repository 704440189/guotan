const signList = () => import('@/pages/signList')
const asign = () => import('@/pages/asign')
const agreement = () => import('@/pages/agreement')
const detail = () => import('@/pages/detail')
const guide = () => import('@/pages/guide')
const report = () => import('@/pages/report')
const reportPower = () => import('@/pages/reportPower')
const dandao = () => import('@/pages/dandao')
const preSheet = () => import('@/pages/preSheet')
const assessmentForm = () => import('@/pages/assessmentForm')
const made = () => import('@/pages/made')
const punchSuccess = () => import('@/pages/punchSuccess')

const HealthEntry = () => import('@/pages/HealthEntry')
const health = () => import('@/pages/health')
const healthForm = () => import('@/pages/healthForm')
const contact = () => import('@/pages/contact')
const mall = () => import('@/pages/mall')
const member = () => import('@/pages/member')

const openDocx = () => import('@/pages/openDocx')
const routes = [
  {
    path: '/',
    redirect: '/healthEntry'
  },
  {
    path: '/healthEntry',
    name: 'healthEntry',
    component: HealthEntry
  },
  {
    path: '/member',
    name: 'member',
    component: member
  },
  {
    path: '/mall',
    name: 'mall',
    component: mall
  },
  {
    path: '/contact',
    name: 'contact',
    component: contact
  },
  {
    path: '/health',
    name: 'health',
    component: health
  },
  {
    path: '/healthForm',
    name: 'healthForm',
    component: healthForm
  },
  {
    path: '/punchSuccess',
    name: 'punchSuccess',
    component: punchSuccess
  },
  {
    path: '/asign',
    name: 'asign',
    component: asign
  },
  {
    path: '/signList',
    name: 'signList',
    component: signList
  },
  {
    path: '/made',
    name: 'made',
    component: made
  },
  {
    path: '/assessmentForm',
    name: 'assessmentForm',
    component: assessmentForm
  },
  {
    path: '/preSheet',
    name: 'preSheet',
    component: preSheet
  },
  {
    path: '/openDocx',
    name: 'openDocx',
    component: openDocx
  },
  {
    path: '/agreement',
    name: 'agreement',
    component: agreement
  },
  {
    path: '/guide',
    name: 'guide',
    component: guide
  },
  {
    path: '/report',
    name: 'report',
    component: report
  },
  {
    path: '/reportPower',
    name: 'reportPower',
    component: reportPower
  },
  {
    path: '/detail',
    name: 'detail',
    component: detail
  },
  {
    path: '/dandao',
    name: 'dandao',
    component: dandao
  }
]
export default routes
