import Vue from 'vue'
import Vuex from 'vuex'
import storage from '@/modules/storage'
import health from './modules/health'

Vue.use(Vuex)

const syncStoreHealth = store => {//vuex操作同步到storge
  store.subscribe((mutation, state) => {
    storage.set(`health`, state.health.healthForm)
  })
}
export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  plugins: [syncStoreHealth],
  modules: {
    health
  }
})
