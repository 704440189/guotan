
const state = {
  token: JSON.parse(sessionStorage.getItem('user')) || null
}
const actions = {
}

export default {
  namespaced: true,
  state,
  actions
}
