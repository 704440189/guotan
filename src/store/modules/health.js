import storage from '@/modules/storage'
const state = {
  healthForm:storage.get('health')||{
    customerBase: {
        customerName:'',
        phone:''
    },
    customerAssessment:{//生活方式
        exerciseFrequency:null,//锻炼频率
        exerciseTime:null,//每次锻炼时间
        exerciseDuration:null,//坚持锻炼时间
        exerciseMode:null,//锻炼方式
        exerciseModeOther:null,//锻炼方式---qita
        smokingStatus:null,//吸烟状况
        smokingVolume:null,//日吸烟量
        smokingAge:null,//开始吸烟年龄
        quitSmokingAge:null,//开始戒烟年龄
        eatingHabits:null,//饮食习惯
        frequencyDrinking:null,//饮酒频率
        alcoholConsumption:null,//日饮酒量
        abstinence:null,//是否戒酒
        abstinenceAge:null,//戒酒年龄
        abstinenceYear:null,//开始饮酒年龄
        drunkennessYear:null,//近一年是否曾醉酒
        typeDrinking:null,//饮酒种类
        typeDrinkingOther:null,//饮酒种类---qita
        pastHistory:null,//既往史
        pastHistoryOther:null,//既往史---qita
        currentSituation:null,//目前症状
        currentSituationOther:null,//目前症状---qita
    },
    familyHistory:{//家族史
        father:null,
        fatherOther:null,
        mother:null,
        motherOther:null,
        brothersAndSisters:null,
        brotherOther:null,
        children:null,
        childrenOther:null,
    },
    hospitalizationHistory:[],//住院史
  }
}

const mutations = {
   editCustomerAssessment(state,data){
     state.healthForm.customerAssessment=Object.assign(state.healthForm.customerAssessment,data)
   },
   editFamilyHistory(state,data){
     state.healthForm.familyHistory=Object.assign(state.healthForm.familyHistory,data)
   },
   editHospitalizationHistory(state,data){
    state.healthForm.hospitalizationHistory=Object.assign(state.healthForm.hospitalizationHistory,data)
   }
}

const actions = {

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
